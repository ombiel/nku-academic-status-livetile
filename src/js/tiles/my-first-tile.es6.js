var {LiveTile,registerTile} = require("@ombiel/exlib-livetile-tools");
var screenLink = require("@ombiel/aek-lib/screen-link");
var statusCSS = require("../../css/academicStatus");
var _ = require("-aek/utils");

class AcademicStatusTile extends LiveTile {

  onReady() {
    // Debounce render so it doesn't get triggered more than once in a 1 sec period
    this.render = _.throttle(this.render.bind(this),1000);
    this.showOriginalFace(()=>{
        this.setOptions();
        this.fetchData();
      });

  }

  fetchData(){
    this.getTileAttributes();
    var url = screenLink("nku-academic-status-livetile/data");
    // This method is required in order for the live tile to work on native
    this.ajax({url:url})
    .done((data)=>{
      this.gpa = data.DATA.GPA;
      this.creditHours = data.DATA.EARNED_CREDITS_HOURS;
      this.standing = data.DATA.ACADEMIC_STANDING;
      this.render();
      this.timer(this.refetchPeriod,this.fetchData.bind(this));
    })
    .fail((err)=>{
      console.log(err);
    }).always(()=>{
      if(this.fetchTimer) { this.fetchTimer.stop(); }
      this.fetchTimer = this.timer(this.refetchPeriod,this.fetchData.bind(this));
    });
  }

  render(){

    var tileAttributes = this.getTileAttributes();

    if(tileAttributes.image || tileAttributes.img){
      var image = tileAttributes.image || tileAttributes.img;
    }

    let gpa = "";
    let creditHours = "";
    let standing = "";

    if (this.gpa && this.gpa !== ""){
      gpa = `<h2 class=${statusCSS.text}>${this.gpaText}${this.gpa}</h2>`;
    }
    if (this.creditHours && this.creditHours !== ""){
      creditHours = `<h2 class=${statusCSS.text}>${this.creditHoursText}${this.creditHours}</h2>`;
    }
    if (this.standing && this.standing !== ""){
      standing = `<h2 class=${statusCSS.text}>${this.standing}</h2>`;
    }

    var face =
    `<div class=${statusCSS.statusFaceContainer} style="background-image:url(${image})">
      <div class=${statusCSS.data}>
        ${standing}
        ${gpa}
        ${creditHours}
      </div>
    </div>`;

  this.flipFace(face);

}

setOptions(){
  var tileAttributes = this.getTileAttributes();
  var statusOptions = tileAttributes.account || {};
  this.gpaText = statusOptions.gpaText || "GPA: ";
  this.creditHoursText = statusOptions.creditHoursText || "Earned Credit Hours: ";
  this.standingText = statusOptions.standingText || "Academic Standing: ";
  this.refetchPeriod = statusOptions.refreshPeriod || 600000;
}

}

registerTile(AcademicStatusTile,"academicStatusTile");
